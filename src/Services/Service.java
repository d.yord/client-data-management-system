package Services;

import Models.Client;

public interface Service {
    void add(Client client);
    void update(Client client);
    void remove(long clientId);
    void getByID(long clientId);
    void getByName(String clientName);
    void getByIndustry(String clientIndustryName);
    void getAll();
    void saveClients();
}
