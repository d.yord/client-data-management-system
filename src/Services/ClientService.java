package Services;

import Exceptions.InvalidClientDataException;
import FileHandlers.FileReader;
import FileHandlers.FileWriter;
import Models.Client;

import java.util.List;

public class ClientService implements Service {
    public static final String SUCCESS = "Success";
    List<Client> clients;
    private final FileWriter fileWriter;

    public ClientService(FileReader fileReader, FileWriter fileWriter) {
        this.clients = fileReader.read();
        this.fileWriter = fileWriter;
    }

    @Override
    public void add(Client client) {
        if (checkIfNotExists(client)) {
            clients.add(client);
            System.out.println(SUCCESS);
        } else {
            System.out.println("Failure: Client exists!");
        }
    }

    @Override
    public void update(Client client) {
        clients.set(getIndex(client.getId()), client);
        System.out.println(SUCCESS);
    }

    @Override
    public void remove(long clientId) {
        clients.remove(getIndex(clientId));
        System.out.println(SUCCESS);
    }

    @Override
    public void getByID(long clientId) {
        System.out.println(clients.get(getIndex(clientId)).toString());
        System.out.println(SUCCESS);
    }

    @Override
    public void getByName(String clientName) {
        List<Client> result = clients.stream().filter(client -> client.getName().contains(clientName)).toList();
        printClientResultList(result);
    }

    @Override
    public void getByIndustry(String clientIndustryName) {
        List<Client> result = clients.stream().filter(client -> client.getIndustry().equals(clientIndustryName)).toList();
        printClientResultList(result);
    }

    @Override
    public void getAll() {
        printClientResultList(clients);
    }

    @Override
    public void saveClients() {
        fileWriter.write(clients);
    }

    private boolean checkIfNotExists(Client client) {
        return clients.stream().noneMatch(client1 -> client1.equals(client));
    }

    private int getIndex(long id) {
        for (int i = 0; i < clients.size(); i++) {
            if (clients.get(i).getId() == id) {
                return i;
            }
        }
        throw new InvalidClientDataException("There is no client with ID: " + id);
    }

    private void printClientResultList(List<Client> result) {
        for (Client c : result) {
            System.out.println(c);
        }
        if (result.isEmpty()) {
            System.out.println("No results");
        } else {
            System.out.println(SUCCESS);
        }
    }
}
