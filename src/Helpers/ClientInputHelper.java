package Helpers;

import Exceptions.InvalidClientDataException;
import Models.Client;

import java.util.Scanner;

public class ClientInputHelper {

    public static final String COMMA_SPACE = ", ";
    public static final int CLIENT_FIELDS_NUMBER = 5;

    public static Client get(){
        Scanner scanner = new Scanner(System.in);
        String clientInput = scanner.nextLine();
        String[] clientData = clientInput.split(COMMA_SPACE);
        if (clientData.length != CLIENT_FIELDS_NUMBER){
            throw new InvalidClientDataException();
        }
        try {
            long clientID = Long.parseLong(clientData[0]);
            String name = clientData[1];
            String industry = clientData[2];
            String contactPerson = clientData[3];
            double revenue = Double.parseDouble(clientData[4]);
            return new Client(clientID, name, industry, contactPerson, revenue);
        }catch (NumberFormatException e){
            throw new InvalidClientDataException();
        }
    }
}
