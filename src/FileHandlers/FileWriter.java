package FileHandlers;

import java.io.Serializable;
import java.util.List;

public interface FileWriter {
    void write(List<? extends Serializable> items);
}
