package FileHandlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

public class JSONFileWriter implements FileWriter {
    public static final String filename = System.getProperty("user.dir") + "\\resources\\clients.json";

    public void write(List<? extends Serializable> items) {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        try {
            objectMapper.writeValue(new File(filename), items);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
