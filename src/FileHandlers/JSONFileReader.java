package FileHandlers;

import Models.Client;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class JSONFileReader implements  FileReader{
    public static final String filename =System.getProperty("user.dir") + "\\resources\\clients.json";
    public List<Client> read(){
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(new File(filename), new TypeReference<List<Client>>(){});
        }catch (IOException ex){
            System.out.println(ex.getMessage());;
            return new ArrayList<>();
        }
    }
}
