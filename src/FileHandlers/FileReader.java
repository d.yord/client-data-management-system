package FileHandlers;

import Models.Client;

import java.io.Serializable;
import java.util.List;

public interface FileReader {
    List<Client> read();
}
