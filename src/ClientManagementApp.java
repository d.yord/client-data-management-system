import FileHandlers.FileWriter;
import FileHandlers.FileReader;
import FileHandlers.JSONFileReader;
import FileHandlers.JSONFileWriter;
import Managers.ClientManager;
import Managers.Manager;
import Services.ClientService;
import Services.Service;

import javax.swing.*;
import java.util.Scanner;
public class ClientManagementApp {
    public static final String OPTIONS = "Please enter options from below categores:\n" +
            "\t1. Options that don't require addtional input:\n" +
            "1.1 Save & Exit\n" +
            "1.2 View Clients\n" +
            "\t 2. Options that require input on same line separated by space:\n" +
            "2.1 Search by Industry + Industry name\n"+
            "2.2 Search by Name + client name\n" +
            "2.3 Search by ID  + client ID number\n" +
            "2.4 Remove Client  + client ID number\n" +
            "\t 3. Options that require input on a new line:\n" +
            "3.1 Update Client\n" +
            "3.2 Add Client\n";
    public static void main(String[] args) {
// Implement file operations for csv/json
        FileWriter fileWriter= new JSONFileWriter();
        FileReader fileReader = new JSONFileReader();
        Service service = new ClientService(fileReader, fileWriter);
        Manager manager = new ClientManager(service);
        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome to the Client Management System");
        displayOptions();
        boolean active = true;
        while (active) {
            String command = scanner.nextLine();
            if(command.equals("Save & Exit")){
                active = false;
            }
            manager.performAction(command.strip());
// Add Client
// 1, Oceanic Enterprises, Finance, Sarah Smith, 500000.00
// Update Client
// 1, Oceanic Enterprises, Tech, Sarah Smith, 750000.00
// View Clients
// Search Industry Tech
// Search ID 1
// Remove Client 1
// Search Name Oceanic
// Save &amp; Exit
        }
    }
    private static void displayOptions(){
        System.out.println(OPTIONS);
    }
}
