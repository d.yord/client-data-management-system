package Managers;

public interface Manager {
    void performAction( String command);
}
