package Managers;

import Helpers.ClientInputHelper;
import Services.Service;

public class ClientManager implements  Manager{
    Service service;
    public ClientManager(Service service) {
        this.service = service;
    }

    @Override
    public void performAction(String command) {
        String[] commandWords = command.strip().split(" ");
        if(commandWords.length <2){
            System.out.println("All commands contain at least 2 words");
        }
        if (command.equals("Save & Exit")){
            service.saveClients();
        }else if(command.equals("View Clients")){
            service.getAll();
        } else if (command.equals("Add Client")) {
            service.add(ClientInputHelper.get());
        } else if (command.equals("Update Client")) {
            service.update(ClientInputHelper.get());
        } else if(commandWords.length >= 3){
            StringBuilder commandArgument = new StringBuilder(commandWords[2]);
            for (int i = 3; i < commandWords.length ; i++) {
                commandArgument.append(commandWords[i]);
            }

            if(commandWords[0].equals("Search") && commandWords[1].equals("Industry")){
                service.getByIndustry(commandArgument.toString());
            } else if (commandWords[0].equals("Search") && commandWords[1].equals("Name")) {
                service.getByName(commandArgument.toString());
            }
            try {
                long currentId = Long.parseLong(commandArgument.toString());
                if (commandWords[0].equals("Remove") && commandWords[1].equals("Client")) {
                    service.remove(currentId);
                } else if (commandWords[0].equals("Search") && commandWords[1].equals("ID")) {
                    service.getByID(currentId);
                }
            }catch (NumberFormatException e){
                System.out.println("ID id a whole number. There is no client with ID: " + commandWords[2]);
            }
        }else {
            System.out.println("There is no such command: " + command);
        }
    }
}
