package Exceptions;

public class InvalidClientDataException extends  RuntimeException{
    public InvalidClientDataException(String message) {
        super(message);
    }
    public InvalidClientDataException(){
        super("The data must be in the following format, each field separated by comma and space:\n" +
                "number ID, name, industry, contact person name, revenue\n\n" +
                "Please note that number ID is whole number and revenue is a number");
    }
}
